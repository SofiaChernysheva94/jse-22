package ru.t1.chernysheva.tm.command.task;

import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private final String NAME = "task-list";

    private final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
