package ru.t1.chernysheva.tm.command.user;

import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class UserListCommand extends AbstractUserCommand {

    private final String NAME = "user-list";

    private final String DESCRIPTION = "Show users list.";

    @Override
    public void execute() {
        System.out.println("[SHOW USERS]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<User> users = getUserService().findAll(sort);
        renderUsers(users);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
