package ru.t1.chernysheva.tm.command.system;

import ru.t1.chernysheva.tm.enumerated.Role;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String NAME = "version";

    private final String ARGUMENT = "-v";

    private final String DESCRIPTION = "Show application version info.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.22.0");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
