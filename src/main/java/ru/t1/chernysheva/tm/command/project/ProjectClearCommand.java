package ru.t1.chernysheva.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private final String NAME = "project-clear";

    private final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


}
