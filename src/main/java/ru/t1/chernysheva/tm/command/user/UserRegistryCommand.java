package ru.t1.chernysheva.tm.command.user;

import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand{

    private final String NAME = "user-registry";

    private final String DESCRIPTION = "User registry.";

    @Override
    public String getName() {
        return  NAME;
    }

    @Override
    public String getDescription() {
        return  DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("[ENTER LOGIN: ]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD: ]");
        final String password = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        final User user = authService.registry(login, password);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
