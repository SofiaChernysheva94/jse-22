package ru.t1.chernysheva.tm.command.user;

import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    private final String NAME = "user-unlock";

    private final String DESCRIPTION = "Unlock user.";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
