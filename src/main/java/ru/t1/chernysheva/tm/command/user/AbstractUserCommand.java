package ru.t1.chernysheva.tm.command.user;

import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.IUserService;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.entity.UserNotFoundException;
import ru.t1.chernysheva.tm.model.User;

import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void renderUsers(final List<User> users) {
        users.forEach(user -> showUser(user));
    }

    protected void showUser(final User user) throws RuntimeException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LOCKED: " + user.isLocked());
    }

    @Override
    public String getArgument() {
        return null;
    }

}
