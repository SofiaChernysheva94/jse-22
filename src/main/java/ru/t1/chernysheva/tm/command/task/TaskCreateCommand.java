package ru.t1.chernysheva.tm.command.task;

import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private final String NAME = "task-create";

    private final String DESCRIPTION = "Task create.";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }



}
