package ru.t1.chernysheva.tm.command.project;

import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private final String NAME = "project-show-by-index";

    private final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
