package ru.t1.chernysheva.tm.command.project;

import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-remove-by-id";

    private final String DESCRIPTION = "Remove project by Id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
