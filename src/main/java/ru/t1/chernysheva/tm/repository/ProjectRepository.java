package ru.t1.chernysheva.tm.repository;

import ru.t1.chernysheva.tm.api.repository.IProjectRepository;
import ru.t1.chernysheva.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(String userId, final String name, final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

}
