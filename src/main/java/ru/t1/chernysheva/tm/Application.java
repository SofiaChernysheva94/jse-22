package ru.t1.chernysheva.tm;

import ru.t1.chernysheva.tm.component.Bootstrap;

public final class Application {

    public static void main(final String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
