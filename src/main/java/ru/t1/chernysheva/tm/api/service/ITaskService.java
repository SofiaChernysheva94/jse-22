package ru.t1.chernysheva.tm.api.service;

import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusById (String userId, String Id, Status status);

    Task changeTaskStatusByIndex (String userId, Integer index, Status status);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);
}
