package ru.t1.chernysheva.tm.api.service;

import ru.t1.chernysheva.tm.api.repository.IRepository;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
