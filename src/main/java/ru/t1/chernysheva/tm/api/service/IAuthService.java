package ru.t1.chernysheva.tm.api.service;

import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;

public interface IAuthService {

    User registry(String login, String password);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(final Role[] roles);

}
