package ru.t1.chernysheva.tm.api.repository;

import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    Task create(String userId, String name, String description);

    Task create(String userId, final String name);

    List<Task> findAllByProjectId(String userId, String projectId);
}
