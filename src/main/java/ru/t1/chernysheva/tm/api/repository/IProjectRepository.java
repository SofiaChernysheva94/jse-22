package ru.t1.chernysheva.tm.api.repository;

import ru.t1.chernysheva.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String name, String s);

}
