package ru.t1.chernysheva.tm.api.service;

import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User>{

    User create(final String login, final String password);

    User create(final String login, String password, final String email);

    User create(final String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(final String login);

    User removeByEmail(final String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName, String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
